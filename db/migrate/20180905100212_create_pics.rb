class CreatePics < ActiveRecord::Migration[5.1]
  def change
    create_table :pics do |t|
      t.references :exam, foreign_key: true
      t.attachment :img
      t.string :title
      t.timestamps
    end
  end
end

class AddFreeToTesting < ActiveRecord::Migration[5.1]
  def change
    add_column :testings, :free, :boolean
  end
end

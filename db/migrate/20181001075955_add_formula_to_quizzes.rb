class AddFormulaToQuizzes < ActiveRecord::Migration[5.1]
  def change
    add_column :quizzes, :isformula, :boolean
  end
end

class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.string :phone
      t.string :body
      t.string :status

      t.timestamps
    end
  end
end

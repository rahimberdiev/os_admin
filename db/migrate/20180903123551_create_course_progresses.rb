class CreateCourseProgresses < ActiveRecord::Migration[5.1]
  def change
    create_table :course_progresses do |t|
      t.references :user, foreign_key: true
      t.integer :course_id
      t.integer :section_id
      t.integer :sc_id
      t.boolean :completed
      t.timestamps
    end
  end
end

class AddFooterAndHeaderToScreencasts < ActiveRecord::Migration[5.1]
  def change
    add_column :screencasts, :footer, :text
    add_column :screencasts, :header, :text
  end
end

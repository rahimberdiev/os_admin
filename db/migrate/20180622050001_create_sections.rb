class CreateSections < ActiveRecord::Migration[5.1]
  def change
    create_table :sections do |t|
      t.integer :no
      t.string :name
      t.string :description
      t.references :course, foreign_key: true
      t.integer :q_count
      t.integer :q_pass
      t.timestamps
    end
  end
end

class CreateSectionProgresses < ActiveRecord::Migration[5.1]
  def change
    create_table :section_progresses do |t|
      t.references :user, foreign_key: true
      t.integer :section_id
      t.integer :score

      t.timestamps
    end
  end
end

class AddFormulaToAnswers < ActiveRecord::Migration[5.1]
  def change
    add_column :answers, :isformula, :boolean
  end
end

class CreateJoinTableCoursesLectors < ActiveRecord::Migration[5.1]
  def change
    create_join_table :courses, :lectors do |t|
      t.index [:course_id, :lector_id]
      # t.index [:lector_id, :course_id]
    end
  end
end

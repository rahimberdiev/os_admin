class CreateExams < ActiveRecord::Migration[5.1]
  def change
    create_table :exams do |t|
      t.integer :no
      t.references :section, foreign_key: true
      t.text :question
      t.text :details
      t.integer :score
      t.timestamps
    end
  end
end

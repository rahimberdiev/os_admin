class AddFormulaToOptions < ActiveRecord::Migration[5.1]
  def change
    add_column :options, :isformula, :boolean
  end
end

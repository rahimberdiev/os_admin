class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.string :txn_id
      t.string :txn_date
      t.string :sum
      t.string :account
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

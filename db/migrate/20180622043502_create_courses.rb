class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.integer :no
      t.string :name
      t.string :description
      t.attachment :logo
      t.references :subject, foreign_key: true
      t.integer :level

      t.timestamps
    end
  end
end

class CreateTestings < ActiveRecord::Migration[5.1]
  def change
    create_table :testings do |t|
      t.string :name
      t.integer :lang
      t.string :description
      t.integer :duration

      t.timestamps
    end
  end
end

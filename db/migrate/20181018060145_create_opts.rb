class CreateOpts < ActiveRecord::Migration[5.1]
  def change
    create_table :opts do |t|
      t.references :prob, foreign_key: true
      t.boolean :isformula
      t.string :value
      t.boolean :correct
      t.timestamps
    end
  end
end

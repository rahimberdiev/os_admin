class CreateProbs < ActiveRecord::Migration[5.1]
  def change
    create_table :probs do |t|
      t.references :testing, foreign_key: true
      t.integer :no
      t.string :question
      t.attachment :img
      t.boolean :isformula
      t.boolean :isinstruction
      t.timestamps
    end
  end
end

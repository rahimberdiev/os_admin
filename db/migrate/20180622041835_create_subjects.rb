class CreateSubjects < ActiveRecord::Migration[5.1]
  def change
    create_table :subjects do |t|
      t.integer :no
      t.integer :lang
      t.string :name
      t.string :description
      t.attachment :logo

      t.timestamps
    end
  end
end

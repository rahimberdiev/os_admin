class CreateQuizzes < ActiveRecord::Migration[5.1]
  def change
    create_table :quizzes do |t|
      t.integer :no
      t.references :screencast, foreign_key: true
      t.text :question
      t.text :details
      t.integer :score
      t.timestamps
    end
  end
end

class ChangeIntForSum < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :summa, :decimal
  end
end

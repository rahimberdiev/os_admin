class AddFormulaToExams < ActiveRecord::Migration[5.1]
  def change
    add_column :exams, :isformula, :boolean
  end
end

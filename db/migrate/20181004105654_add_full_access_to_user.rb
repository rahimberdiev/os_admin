class AddFullAccessToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :full_access, :boolean
  end
end

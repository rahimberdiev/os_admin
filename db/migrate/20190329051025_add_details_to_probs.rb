class AddDetailsToProbs < ActiveRecord::Migration[5.1]
  def change
    add_column :probs, :details, :string
  end
end

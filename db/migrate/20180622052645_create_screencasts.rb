class CreateScreencasts < ActiveRecord::Migration[5.1]
  def change
    create_table :screencasts do |t|
      t.integer :no
      t.string :name
      t.attachment :logo
      t.attachment :video
      t.references :section, foreign_key: true
      t.integer :q_count
      t.boolean :free

      t.timestamps
    end
  end
end

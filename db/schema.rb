# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181018101332) do

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "answers", force: :cascade do |t|
    t.string "value"
    t.integer "exam_id"
    t.boolean "correct"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "isformula"
    t.index ["exam_id"], name: "index_answers_on_exam_id"
  end

  create_table "course_progresses", force: :cascade do |t|
    t.integer "user_id"
    t.integer "course_id"
    t.integer "section_id"
    t.integer "sc_id"
    t.boolean "completed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_course_progresses_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.integer "no"
    t.string "name"
    t.string "description"
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.integer "subject_id"
    t.integer "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subject_id"], name: "index_courses_on_subject_id"
  end

  create_table "courses_lectors", id: false, force: :cascade do |t|
    t.integer "course_id", null: false
    t.integer "lector_id", null: false
    t.index ["course_id", "lector_id"], name: "index_courses_lectors_on_course_id_and_lector_id"
  end

  create_table "exams", force: :cascade do |t|
    t.integer "no"
    t.integer "section_id"
    t.text "question"
    t.text "details"
    t.integer "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "isformula"
    t.index ["section_id"], name: "index_exams_on_section_id"
  end

  create_table "imgs", force: :cascade do |t|
    t.integer "quiz_id"
    t.string "img_file_name"
    t.string "img_content_type"
    t.integer "img_file_size"
    t.datetime "img_updated_at"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["quiz_id"], name: "index_imgs_on_quiz_id"
  end

  create_table "lectors", force: :cascade do |t|
    t.string "fullname"
    t.string "level"
    t.string "degree"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.string "phone"
    t.string "body"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "options", force: :cascade do |t|
    t.string "value"
    t.integer "quiz_id"
    t.boolean "correct"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "isformula"
    t.index ["quiz_id"], name: "index_options_on_quiz_id"
  end

  create_table "opts", force: :cascade do |t|
    t.integer "prob_id"
    t.boolean "isformula"
    t.string "value"
    t.boolean "correct"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["prob_id"], name: "index_opts_on_prob_id"
  end

  create_table "payments", force: :cascade do |t|
    t.string "txn_id"
    t.string "txn_date"
    t.integer "sum"
    t.string "account"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_payments_on_user_id"
  end

  create_table "pics", force: :cascade do |t|
    t.integer "exam_id"
    t.string "img_file_name"
    t.string "img_content_type"
    t.integer "img_file_size"
    t.datetime "img_updated_at"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["exam_id"], name: "index_pics_on_exam_id"
  end

  create_table "probs", force: :cascade do |t|
    t.integer "testing_id"
    t.integer "no"
    t.string "question"
    t.string "img_file_name"
    t.string "img_content_type"
    t.integer "img_file_size"
    t.datetime "img_updated_at"
    t.boolean "isformula"
    t.boolean "isinstruction"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["testing_id"], name: "index_probs_on_testing_id"
  end

  create_table "quizzes", force: :cascade do |t|
    t.integer "no"
    t.integer "screencast_id"
    t.text "question"
    t.text "details"
    t.integer "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "isformula"
    t.index ["screencast_id"], name: "index_quizzes_on_screencast_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.integer "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_schools_on_region_id"
  end

  create_table "screencasts", force: :cascade do |t|
    t.integer "no"
    t.string "name"
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.string "video_file_name"
    t.string "video_content_type"
    t.integer "video_file_size"
    t.datetime "video_updated_at"
    t.integer "section_id"
    t.integer "q_count"
    t.boolean "free"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "footer"
    t.text "header"
    t.index ["section_id"], name: "index_screencasts_on_section_id"
  end

  create_table "section_progresses", force: :cascade do |t|
    t.integer "user_id"
    t.integer "section_id"
    t.integer "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_section_progresses_on_user_id"
  end

  create_table "sections", force: :cascade do |t|
    t.integer "no"
    t.string "name"
    t.string "description"
    t.integer "course_id"
    t.integer "q_count"
    t.integer "q_pass"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_sections_on_course_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.integer "no"
    t.integer "lang"
    t.string "name"
    t.string "description"
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "testings", force: :cascade do |t|
    t.string "name"
    t.integer "lang"
    t.string "description"
    t.integer "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "free"
  end

  create_table "user_courses", force: :cascade do |t|
    t.integer "user_id"
    t.integer "course_id"
    t.integer "current_sc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_user_courses_on_course_id"
    t.index ["user_id"], name: "index_user_courses_on_user_id"
  end

  create_table "user_sections", force: :cascade do |t|
    t.integer "user_id"
    t.integer "section_id"
    t.integer "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_sections_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.integer "lang"
    t.string "username"
    t.string "password_digest"
    t.string "fullname"
    t.string "email"
    t.string "phone"
    t.integer "region"
    t.integer "district"
    t.string "school"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.string "token"
    t.string "pin"
    t.datetime "token_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "approved"
    t.boolean "full_access"
    t.boolean "aggree"
    t.index ["token", "token_created_at"], name: "index_users_on_token_and_token_created_at"
  end

end

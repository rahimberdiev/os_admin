class SectionsController < ApplicationController
  before_action :set_section, only: [:show, :edit, :update, :destroy]
  before_action :set_course, only: [:index,:new, :create, :destroy]

  def index
    @sections = @course.sections.order(:no)
  end

  def show
  end

  def new
    @section = @course.sections.new
    @section.q_pass = 10 
    @section.no =@course.sections.maximum(:no) ? @course.sections.maximum(:no)+1 : 1
  end

  def edit
  end

  def create
    @section = @course.sections.new(section_params)

    respond_to do |format|
      if @section.save
        format.html { redirect_to @section, notice: 'Section was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /sections/1
  # PATCH/PUT /sections/1.json
  def update
    respond_to do |format|
      if @section.update(section_params)
        format.html { redirect_to @section, notice: 'Section was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section.destroy
    respond_to do |format|
      format.html { redirect_to @course, notice: 'Section was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_section
      @section = Section.find(params[:id])
    end
    def set_course
      @course = Course.find(params[:course_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def section_params
      params.require(:section).permit(:no, :name, :description, :course_id, :q_count, :q_pass)
    end
end

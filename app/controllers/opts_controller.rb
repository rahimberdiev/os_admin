class OptsController < ApplicationController
  before_action :set_opt, only: [:show, :edit, :update, :destroy]
  before_action :set_prob, only: [:index,:new, :create, :destroy]

  def index
    @opts = @prob.opts
  end

  def show
  end

  def new
    @opt = @prob.opts.new
    @opt.isformula = false
  end

  def edit
  end

  def create
    @opt = @prob.opts.new(opt_params)

    respond_to do |format|
      if @opt.save
        format.html { redirect_to @opt.prob, notice: 'Option was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @opt.update(opt_params)
        format.html { redirect_to @opt.prob, notice: 'Option was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @opt.destroy
    respond_to do |format|
      format.html { redirect_to @prob, notice: 'Option was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_opt
      @opt = Opt.find(params[:id])
    end
    def set_prob
      @prob = Prob.find(params[:prob_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def opt_params
      params.require(:opt).permit(:mark, :value, :prob_id, :correct,:isformula)
    end
end

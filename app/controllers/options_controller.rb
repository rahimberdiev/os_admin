class OptionsController < ApplicationController
  before_action :set_option, only: [:show, :edit, :update, :destroy]
  before_action :set_quiz, only: [:index,:new, :create, :destroy]

  # GET /options
  # GET /options.json
  def index
    @options = @quiz.options
  end

  # GET /options/1
  # GET /options/1.json
  def show
  end

  # GET /options/new
  def new
    @option = @quiz.options.new
    @option.isformula = false
  end

  # GET /options/1/edit
  def edit
  end

  # POST /options
  # POST /options.json
  def create
    @option = @quiz.options.new(option_params)

    respond_to do |format|
      if @option.save
        format.html { redirect_to @option.quiz, notice: 'Option was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /options/1
  # PATCH/PUT /options/1.json
  def update
    respond_to do |format|
      if @option.update(option_params)
        format.html { redirect_to @option.quiz, notice: 'Option was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /options/1
  # DELETE /options/1.json
  def destroy
    @option.destroy
    respond_to do |format|
      format.html { redirect_to @quiz, notice: 'Option was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_option
      @option = Option.find(params[:id])
    end
    def set_quiz
      @quiz = Quiz.find(params[:quiz_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def option_params
      params.require(:option).permit(:mark, :value, :quiz_id, :correct,:isformula)
    end
end

class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy,:add_course,:remove_course]

  # GET /users
  # GET /users.json
  def index
    @users = User.order(id: :desc)

  end

  # GET /users/1
  # GET /users/1.json
  def show
    @courses = Course.all
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.password = user_params[:password]

    respond_to do |format|
      @user.email.downcase!
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def add_course
    @user.userCourses.create(course_id: params[:course_id])

    respond_to do |format|
      format.html { redirect_to @user }
    end
  end
  def remove_course 
    @user.userCourses.destroy(params[:course_id])
    respond_to do |format|
      format.html { redirect_to @user }
    end
  end
  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        if params[:user]["password"]
          @user.password = params[:user]["password"]
          @user.save
        end
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:fullname, :username, :lang, :full_access, :approved,:email,:password)
    end
end

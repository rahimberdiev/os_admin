class TestingsController < ApplicationController
  before_action :set_testing, only: [:show, :edit, :update, :destroy]

  def index
    @testings = Testing.all
  end

  def show
  end

  def new
    @testing = Testing.new
    @testing.lang=0
  end

  def edit
  end

  def create
    @testing = Testing.new(testing_params)
    respond_to do |format|
      if @testing.save
        format.html { redirect_to @testing, notice: 'Testing was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @testing.update(testing_params)
        format.html { redirect_to @testing, notice: 'Testing was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @testing.destroy
    respond_to do |format|
      format.html { redirect_to testings_url, notice: 'Testing was successfully destroyed.' }
    end
  end

  private
    def set_testing
      @testing = Testing.find(params[:id])
    end

    def testing_params
      params.require(:testing).permit(:lang,:name, :description,:duration,:free)
    end
end

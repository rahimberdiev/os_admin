class QuizzesController < ApplicationController
  before_action :set_quiz, only: [:show, :edit, :update, :destroy]
  before_action :set_screencast, only: [:index,:new, :create, :destroy]

  # GET /quizzes
  # GET /quizzes.json
  def index
    @quizzes = @screencast.quizzes
  end

  # GET /quizzes/1
  # GET /quizzes/1.json
  def show
  end

  # GET /quizzes/new
  def new
    @quiz = @screencast.quizzes.new
    @quiz.no = @screencast.quizzes.maximum(:no) ? @screencast.quizzes.maximum(:no) : 1 
    @quiz.isformula = false
  end

  # GET /quizzes/1/edit
  def edit
  end

  # POST /quizzes
  # POST /quizzes.json
  def create
    @quiz = @screencast.quizzes.new(quiz_params)

    respond_to do |format|
      if @quiz.save
        opts = params[:quiz][:opts].split("\n").reject(&:blank?)
        opts.each do |opt|
          @quiz.options.create(value:opt,correct:false)
        end
        format.html { redirect_to @quiz, notice: 'Quiz was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /quizzes/1
  # PATCH/PUT /quizzes/1.json
  def update
    respond_to do |format|
      if @quiz.update(quiz_params)
        format.html { redirect_to @quiz, notice: 'Quiz was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /quizzes/1
  # DELETE /quizzes/1.json
  def destroy
    @quiz.destroy
    respond_to do |format|
      format.html { redirect_to @screencast, notice: 'Quiz was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quiz
      @quiz = Quiz.find(params[:id])
    end
    def set_screencast
      @screencast = Screencast.find(params[:screencast_id])
    end


    # Never trust parameters from the scary internet, only allow the white list through.
    def quiz_params
      params.require(:quiz).permit(:no, :screencast_id, :question, :details, :score,:isformula)
    end
end

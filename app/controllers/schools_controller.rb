class SchoolsController < ApplicationController
  before_action :set_school, only: [:show, :edit, :update, :destroy]
  before_action :set_region, only: [:index,:new, :create, :destroy]

  def index
    @schools = @region.schools
  end

  def show
  end

  def new
    @school = @region.schools.new
  end

  def edit
  end

  def create
    @school = @region.schools.new(school_params)

    respond_to do |format|
      if @school.save
        format.html { redirect_to @region, notice: 'School was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @school.update(school_params)
        format.html { redirect_to @region, notice: 'School was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @school.destroy
    respond_to do |format|
      format.html { redirect_to @region, notice: 'School was successfully destroyed.' }
    end
  end

  private
    def set_school
      @school = School.find(params[:id])
    end
    def set_region
      @region = Region.find(params[:region_id])
    end

    def school_params
      params.require(:school).permit(:name)
    end
end

class ProbsController < ApplicationController
  before_action :set_prob, only: [:show, :edit, :update, :destroy]
  before_action :set_testing, only: [:index,:new, :create, :destroy]

  def index
    @probs = @testing.probs.order(:no)
  end

  def show
  end

  def new
    @prob = @testing.probs.new
    @prob.no = @testing.probs.maximum(:no)
    @prob.isformula = false
  end

  def edit
  end

  def create
    @prob = @testing.probs.new(prob_params)

    respond_to do |format|
      if @prob.save
        opts = params[:prob][:options].split("\n").reject(&:blank?)
        opts.each do |opt|
          @prob.opts.create(value:opt,correct:false)
        end
        format.html { redirect_to @testing, notice: 'Prob was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @prob.update(prob_params)
        format.html { redirect_to @prob.testing, notice: 'Prob was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @prob.destroy
    respond_to do |format|
      format.html { redirect_to @testing, notice: 'Prob was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_prob
      @prob = Prob.find(params[:id])
    end
    def set_testing
      @testing = Testing.find(params[:testing_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def prob_params
      params.require(:prob).permit(:no, :testing_id, :question, :isformula,:isinstruction,:img, :details)
    end
end

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_admin!
  before_action :init_admin

  private
    # Use callbacks to share common setup or constraints between actions.
    def init_admin
      admin = Admin.find_by(email:"apgrade@apgrade.kg")
      if !admin
        admin = Admin.new(email:"apgrade@apgrade.kg")
        admin.password = "qwerty1@31"
        admin.save
      end
    end
end

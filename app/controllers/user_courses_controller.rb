class UserCoursesController < ApplicationController
  before_action :set_user_course, only: [:show, :edit, :update, :destroy]
  before_action :set_user
  # GET /user_courses
  # GET /user_courses.json
  def index
    @user_courses = @user.userCourses.all
  end

  # GET /user_courses/1
  # GET /user_courses/1.json
  def show
  end

  # GET /user_courses/new
  def new
    @user_course = @user.userCourses.new
  end

  # GET /user_courses/1/edit
  def edit
  end

  # POST /user_courses
  # POST /user_courses.json
  def create
    @user_course = @user.userCourses.new(user_course_params)

    respond_to do |format|
      if @user_course.save
        format.html { redirect_to @user, notice: 'User course was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /user_courses/1
  # PATCH/PUT /user_courses/1.json
  def update
    respond_to do |format|
      if @user_course.update(user_course_params)
        format.html { redirect_to @user, notice: 'User course was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /user_courses/1
  # DELETE /user_courses/1.json
  def destroy
    @user_course.destroy
    respond_to do |format|
      format.html { redirect_to @user, notice: 'User course was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_course
      @user_course = UserCourse.find(params[:id])
    end
    def set_user
      @user = params[:user_id]&&User.find(params[:user_id])||
        User.find(@user_course.user_id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_course_params
      params.require(:user_course).permit(:user_id, :course_id)
    end
end

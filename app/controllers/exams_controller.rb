class ExamsController < ApplicationController
  before_action :set_exam, only: [:show, :edit, :update, :destroy]
  before_action :set_section, only: [:index,:new, :create, :destroy]

  # GET /exams
  # GET /exams.json
  def index
    @exams = @section.exams
  end

  # GET /exams/1
  # GET /exams/1.json
  def show
  end

  # GET /exams/new
  def new
    @exam = @section.exams.new
    @exam.score = 1
    @exam.no = @section.exams.maximum(:no)
    @exam.isformula = false
  end

  # GET /exams/1/edit
  def edit
  end

  # POST /exams
  # POST /exams.json
  def create
    @exam = @section.exams.new(exam_params)

    respond_to do |format|
      if @exam.save
        opts = params[:exam][:opts].split("\n").reject(&:blank?)
        opts.each do |opt|
          @exam.answers.create(value:opt,correct:false)
        end
        format.html { redirect_to @exam, notice: 'Exam was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /exams/1
  # PATCH/PUT /exams/1.json
  def update
    respond_to do |format|
      if @exam.update(exam_params)
        format.html { redirect_to @exam, notice: 'Exam was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /exams/1
  # DELETE /exams/1.json
  def destroy
    @exam.destroy
    respond_to do |format|
      format.html { redirect_to @section, notice: 'Exam was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exam
      @exam = Exam.find(params[:id])
    end
    def set_section
      @section = Section.find(params[:section_id])
    end


    # Never trust parameters from the scary internet, only allow the white list through.
    def exam_params
      params.require(:exam).permit(:no, :section_id, :question, :details, :score,:isformula)
    end
end

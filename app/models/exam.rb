class Exam < ApplicationRecord
  attr_accessor :opts
  belongs_to :section
  has_many :answers, dependent: :destroy
  has_many :pics, dependent: :destroy
end

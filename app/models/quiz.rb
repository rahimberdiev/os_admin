class Quiz < ApplicationRecord
  attr_accessor :opts
  belongs_to :screencast
  has_many :options, dependent: :destroy
  has_many :imgs, dependent: :destroy
end

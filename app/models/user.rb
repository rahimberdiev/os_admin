class User < ApplicationRecord
  has_attached_file :photo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/

  has_secure_password
  has_many :userCourses,:dependent => :destroy
  has_many :payments
  has_many :courseProgresses, :dependent => :destroy
  has_many :sectionProgresses, :dependent => :destroy
end

Rails.application.routes.draw do
  devise_for :admins
  root to: "subjects#index"

  resources :lectors 

  resources :users do 
    resources :user_courses, only: [:index,:new,:create,:destroy]
  end

  get '/add_course/:id/:course_id', to: 'users#add_course', as: 'add_course'
  get '/remove_course/:id/:course_id', to: 'users#remove_course', as: 'remove_course'

  resources :user_courses, only: [:show, :edit, :update]


  resources :subjects do 
    resources :courses, only: [:index,:new,:create, :destroy] 
  end
  get '/add_lector/:course_id/:lector_id', to: 'courses#add_lector', as: 'add_lector'
  get '/remove_lector/:course_id/:lector_id', to: 'courses#remove_lector', as: 'remove_lector'
  resources :courses, only: [:show, :edit, :update] do
    resources :sections, only: [:index,:new, :create, :destroy]
  end
  resources :sections, only: [:show, :edit, :update] do
    resources :screencasts, only: [:index,:new, :create, :destroy]
    resources :exams, only: [:index,:new, :create, :destroy]
  end
  resources :screencasts, only: [:show, :edit, :update] do 
    resources :quizzes, only: [:index,:new, :create, :destroy]
  end
  get "screencast/convert_video/:id" ,to: "screencasts#convert_video", as: "screencast_convert_video"
  resources :quizzes, only: [:show, :edit, :update] do
    resources :options, only: [:index,:new, :create, :destroy]
    resources :imgs, only: [:index,:new, :create, :destroy]
  end
  resources :options, only: [:show, :edit, :update]
  resources :imgs, only: [:show, :edit, :update]

  resources :exams, only: [:show, :edit, :update] do
    resources :answers, only: [:index,:new, :create, :destroy]
    resources :pics, only: [:index,:new, :create, :destroy]
  end
  resources :answers, only: [:show, :edit, :update]
  resources :pics, only: [:show, :edit, :update]

  resources :regions do
    resources :schools, only: [:index,:new, :create, :destroy]
  end
  resources :schools, only: [:show, :edit, :update]
  
  resources :testings do
    resources :probs, only: [:index,:new, :create, :destroy]
  end
  resources :probs, only: [:show, :edit, :update] do 
    resources :opts, only: [:index,:new, :create, :destroy]
  end
  resources :opts, only: [:show, :edit, :update] 
end

require 'test_helper'

class ScreencastsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @screencast = screencasts(:one)
  end

  test "should get index" do
    get screencasts_url
    assert_response :success
  end

  test "should get new" do
    get new_screencast_url
    assert_response :success
  end

  test "should create screencast" do
    assert_difference('Screencast.count') do
      post screencasts_url, params: { screencast: { course_id: @screencast.course_id, logo: @screencast.logo, name: @screencast.name, pass_score: @screencast.pass_score, questions_count: @screencast.questions_count, video: @screencast.video } }
    end

    assert_redirected_to screencast_url(Screencast.last)
  end

  test "should show screencast" do
    get screencast_url(@screencast)
    assert_response :success
  end

  test "should get edit" do
    get edit_screencast_url(@screencast)
    assert_response :success
  end

  test "should update screencast" do
    patch screencast_url(@screencast), params: { screencast: { course_id: @screencast.course_id, logo: @screencast.logo, name: @screencast.name, pass_score: @screencast.pass_score, questions_count: @screencast.questions_count, video: @screencast.video } }
    assert_redirected_to screencast_url(@screencast)
  end

  test "should destroy screencast" do
    assert_difference('Screencast.count', -1) do
      delete screencast_url(@screencast)
    end

    assert_redirected_to screencasts_url
  end
end
